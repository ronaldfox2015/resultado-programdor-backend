<?php

// Routes

$app->get('/', App\Action\HomeAction::class)
        ->setName('homepage');

$app->get('/listado', App\Action\ListadoAction::class)
        ->setName('listadopage');

$app->post('/listado', App\Action\ListadoAction::class)
        ->setName('listadopage');

$app->get('/detalle/id/{id}', App\Action\DetalleAction::class)
        ->setName('detallepage');

$app->get('/busqueda', App\Action\BusquedaAction::class)
        ->setName('busquedapage');


