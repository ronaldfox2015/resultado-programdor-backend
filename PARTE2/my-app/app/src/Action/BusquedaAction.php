<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\Action;

use Slim\Views\Twig;
use Psr\Log\LoggerInterface;
use Slim\Http\Request;
use Slim\Http\Response;
use SimpleXMLElement;

/**
 * Description of BusquedaAction
 *
 * @author ronald
 */
final class BusquedaAction
{

    /**
     *
     * @var type 
     */
    private $view;

    /**
     *
     * @var type 
     */
    private $logger;

    /**
     * 
     * @param Twig $view
     * @param LoggerInterface $logger
     */
    public function __construct(Twig $view, LoggerInterface $logger)
    {
        $this->view = $view;
        $this->logger = $logger;
    }

    /**
     * 
     * @param Request $request
     * @param Response $response
     * @param type $args
     * @return Response
     */
    public function __invoke(Request $request, Response $response, $args)
    {
        $params = $request->getParams();
        $response->withHeader('Content-type', 'application/xml');
        $this->logger->info("Home page action dispatched");
        $data = file_get_contents(dirname(__FILE__)
                . '/../../../public/elements/employees.json');
        $products = json_decode($data, true);
        $products = $this->buscarPalabra($products, $params['salario1'], $params['salario2'], 'salary');
        $view = array(
            'data' => $products);

        $this->view->render($response, 'listar.xml', $view);
        return $response;
    }

    /**
     * Funcion que evalua el rango de precios
     * @param type $data
     * @param type $input
     * @param type $valor
     * @return type
     */
    private function buscarPalabra($data, $valor1, $valor2, $tipo)
    {
        $result = array_filter($data, function ($item) use ($tipo, $valor1, $valor2) {
            $salario = str_replace("$", "", $item[$tipo]);
            $salario = str_replace(",", "", $salario);
            $salario = floatval($salario);
            $valor1 = floatval($valor1);
            $valor2 = floatval($valor2);
            if ($valor1 <= $salario && $valor2 >=$salario ) {
                return true;
            }
            return false;
        });
        return $result;
    }

}
