<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\Action;

use Slim\Views\Twig;
use Psr\Log\LoggerInterface;
use Slim\Http\Request;
use Slim\Http\Response;

/**
 * Description of ListadoAction
 *
 * @author ronald
 */
final class ListadoAction
{

    /**
     *
     * @var type 
     */
    private $view;

    /**
     *
     * @var type 
     */
    private $logger;

    public function __construct(Twig $view, LoggerInterface $logger)
    {
        $this->view = $view;
        $this->logger = $logger;
    }

    public function __invoke(Request $request, Response $response, $args)
    {
        $allPostVars = $request->getParsedBody();

        $this->logger->info("Home page action dispatched");
        $data = file_get_contents(dirname(__FILE__)
                . '/../../../public/elements/employees.json');
        $products = json_decode($data, true);
        if (isset($allPostVars['email'])) {
            $products = $this->buscarPalabra($products, $allPostVars['email'], 'email');
        }
        $view = array(
            'data' => $products);
        $this->view->render($response, 'listado.twig', $view);
        return $response;
    }

    private function buscarPalabra($data, $input, $valor)
    {
        $result = array_filter($data, function ($item) use ($input,$valor) {
            if (stripos($item[$valor], $input) !== false) {
                return true;
            }
            return false;
        });
        return $result;
    }

}
