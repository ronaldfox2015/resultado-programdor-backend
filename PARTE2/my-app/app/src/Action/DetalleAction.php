<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\Action;

use Slim\Views\Twig;
use Psr\Log\LoggerInterface;
use Slim\Http\Request;
use Slim\Http\Response;

/**
 * Description of DetalleAction
 *
 * @author ronald
 */
final class DetalleAction
{

    /**
     *
     * @var type 
     */
    private $view;

    /**
     *
     * @var type 
     */
    private $logger;

    /**
     * 
     * @param Twig $view
     * @param LoggerInterface $logger
     */
    public function __construct(Twig $view, LoggerInterface $logger)
    {
        $this->view = $view;
        $this->logger = $logger;
    }

    /**
     * 
     * @param Request $request
     * @param Response $response
     * @param type $args
     * @return Response
     */
    public function __invoke(Request $request, Response $response, $args)
    {
        if (isset($args['id'])) {
            $this->logger->info("Home page action dispatched");
            $data = file_get_contents(dirname(__FILE__)
                    . '/../../../public/elements/employees.json');
            $products = json_decode($data, true);
            $products = $this->buscarPalabra($products, $args['id'], 'id');
            $view = array(
                'data' => array(
                    'name' => '',
                    'email' => '',
                    'phone' => '',
                    'address' => '',
                    'position' => '',
                    'salary' => '',
                ),
                'skills' => array()
            );
            if (count($products) > 0) {
                foreach ($products as $key => $value) {
                    $view['data']['name'] = $value['name'];
                    $view['data']['email'] = $value['email'];
                    $view['data']['phone'] = $value['phone'];
                    $view['data']['address'] = $value['address'];
                    $view['data']['position'] = $value['position'];
                    $view['data']['salary'] = $value['salary'];
                    $view['skills'] = $value['skills'];
                }
            }
            $this->view->render($response, 'detalle.twig', $view);
            return $response;
        }
    }

    /**
     * 
     * @param type $data
     * @param type $input
     * @param type $valor
     * @return type
     */
    private function buscarPalabra($data, $input, $valor)
    {
        $result = array_filter($data, function ($item) use ($input, $valor) {
            if (stripos($item[$valor], $input) !== false) {
                return true;
            }
            return false;
        });
        return $result;
    }

}
